package elro1616students.ju.lakebaikal;

import java.util.ArrayList;
import java.util.List;

class Payments {
    private static final Payments ourInstance = new Payments();

    static Payments getInstance() {
        return ourInstance;
    }

    public List<Payment> payments;

    private Payments() {
        payments = new ArrayList<>();
    }


}
