package elro1616students.ju.lakebaikal;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;




public class MainActivity extends AppCompatActivity {

    private EditText editTextTitle;
    private EditText editTextMessage;

    static final String CHANNEL_1_ID = "channel1";

    final HomeFragment homeFragment = new HomeFragment();
    final BillsFragment billsFragment = new BillsFragment();
    final SettingsFragment settingsFragment = new SettingsFragment();
    FirebaseAuth mAuth;
    Fragment currentFragment = null;
    private static final int PERMISSIONS_REQUEST = 100;
    BluetoothAdapter btAdapter;
    private static final int REQUEST_ENABLE_BT = 200;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getApplicationContext();



        //createNotificationChannel();

        setContentView(R.layout.activity_main);
        BottomNavigationView navigationView = findViewById(R.id.bottomNavBar);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.home) {
                    setFragment(homeFragment, settingsFragment, billsFragment);
                    return true;
                } else if (id == R.id.bills){
                    setFragment(billsFragment,settingsFragment,homeFragment);
                    return true;
                } else if (id == R.id.settings) {
                    setFragment(settingsFragment,homeFragment,billsFragment);
                    return true;
                }
                return false;
            }
        });

        if(savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame, homeFragment).hide(homeFragment);
            fragmentTransaction.add(R.id.frame, settingsFragment).hide(settingsFragment);
            fragmentTransaction.add(R.id.frame, billsFragment).hide(billsFragment);
            fragmentTransaction.commit();
            navigationView.setSelectedItemId(R.id.home);
        }


    }

  /*  public  void notis(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.scanlogo)
                .setContentTitle("Payment sent")
                .setContentText("100")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(uniqueId, builder.build());
    }*/






    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.hide(billsFragment);
        fragmentTransaction.hide(homeFragment);
        fragmentTransaction.hide(settingsFragment);
        fragmentTransaction.show(currentFragment);
        fragmentTransaction.commit();
        // Checks the orientation of the screen
    }

    private void setFragment(Fragment fragment, Fragment oldFragment1, Fragment oldFragment2) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.hide(oldFragment1);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(oldFragment2);
        fragmentTransaction.commit();
        currentFragment = fragment;
    }




}



