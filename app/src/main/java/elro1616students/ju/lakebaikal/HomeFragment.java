package elro1616students.ju.lakebaikal;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gigamole.library.PulseView;

import static elro1616students.ju.lakebaikal.MainActivity.CHANNEL_1_ID;

public class HomeFragment extends Fragment {

    PulseView pulseView;
    View view;
    GPSTracker gpsTracker;
    BluetoothAdapter btAdapter;
    Button btn_pay;
    private  NotificationManagerCompat notificationManager;
    private int uniqueNotifId = 12541;


    private static final int REQUEST_ENABLE_BT = 200;
    private static final int REQUEST_BT_PERMISSION = 201;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        // get BT adapter
        createNotificationChannel();

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.BLUETOOTH}, REQUEST_BT_PERMISSION);
        } else {
            EnableBT();
        }

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter);


        //gpsTracker = new GPSTracker(getContext(),btHelper);

        DatabaseHelper.getInstance().StartUpdatingTolls(new Listener() {
            @Override
            public void onResponse(Object response) {
            }
        });

        //gpsTracker.setProximityAlerts();

        view = inflater.inflate(R.layout.fragment_home, container, false);

        pulseView = view.findViewById(R.id.pv);
        pulseView.startPulse();

        return view;
    }

    void startBT () {
        BluetoothHelper btHelper = new BluetoothHelper(getContext(), btAdapter, new Listener() {
            @Override
            public void onResponse(Object response) {
                notificationManager = NotificationManagerCompat.from(getContext());

                String cost = String.valueOf(response);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), CHANNEL_1_ID)
                        .setSmallIcon(R.drawable.lakelogo)
                        .setContentTitle("Payment sent")
                        .setContentText(cost+"kr")
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

                notificationManager.notify(uniqueNotifId, builder.build());
            }
        });
        btHelper.scanLeDevice(true);
    }

    void EnableBT() {
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null) {
            // Device doesn't support Bluetooth
            System.out.println("BT FUNKAR INTE");
        } else {
            System.out.println("BT FUNKAR");
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                startBT();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                startBT();
            } else {
                Process.killProcess(Process.myPid());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_BT_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                EnableBT();
            } else {
                Process.killProcess(Process.myPid());
            }
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    if (state == BluetoothAdapter.STATE_OFF) {
                        if (!btAdapter.isEnabled()) {
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent, 641);
                        }
                    }
                }
            }
        };

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mReceiver);
    }

    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_1_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
