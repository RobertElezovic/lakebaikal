package elro1616students.ju.lakebaikal;

public class Toll {

    public Double latitude;
    public Double longitude;
    public String road;
    public Float amount;
    public Boolean paid;
    public String uuid;

    public Toll(Double latitude, Double longitude, String road, Float amount, String uuid) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.road = road;
        this.amount = amount;
        this.paid = false;
        this.uuid = uuid;
    }

    public Toll() {
        paid = false;
    }
}
