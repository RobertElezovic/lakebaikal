package elro1616students.ju.lakebaikal;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;

public class ChoosePaymentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosepayment);
    }

    public void ChoosePaymentOnClick (View view) {
        User.getInstance().HasPaymentMethod = true;
        DatabaseHelper.getInstance().writeUserToDb(User.getInstance());
    }
}
