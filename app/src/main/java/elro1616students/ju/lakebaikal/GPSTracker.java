package elro1616students.ju.lakebaikal;


import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.security.interfaces.DSAPublicKey;

public class GPSTracker extends Service {

    private static final String TAG = GPSTracker.class.getSimpleName();
    private Context context;
    private BluetoothHelper bluetoothHelper;

    public GPSTracker(Context context, BluetoothHelper bluetoothHelper) {
        this.context = context;
        this.bluetoothHelper = bluetoothHelper;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setProximityAlerts() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        checkTollProximity(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                checkTollProximity(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

    }

    void checkTollProximity (Location currentLocation) {
        for (Toll toll : Tolls.getInstance().tolls) {
            Location tollLocation = new Location("");
            tollLocation.setLongitude(toll.longitude);
            tollLocation.setLatitude(toll.latitude);
            if(currentLocation.distanceTo(tollLocation) <= 100) {
                if(!toll.paid) {
                    Toast.makeText(context, "Proximity alert", Toast.LENGTH_LONG).show();
                    bluetoothHelper.scanLeDevice(true);
                    toll.paid = true;
                }
            } else {
                bluetoothHelper.scanLeDevice(false);
                if(toll.paid) {
                    toll.paid = false;
                }
            }
        }
    }
}