package elro1616students.ju.lakebaikal;

import android.app.DatePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class BillsFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    View view;
    ListView listView;
    Button fromDateButton;
    Button toDateButton;
    Date fromDate;
    Date toDate;
    int  year_x = 2019, month_x = 04, day_x = 17;

    public BillsFragment() {
        // Required empty public constructor
    }

    public String checkDigit(int number)
    {
        return number<=9?"0"+number:String.valueOf(number);
    }

    public void DatePickOnClick (View button) {
        if(button.getId() == R.id.FromDatePicker) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        String yearString = checkDigit(year);
                        String monthString = checkDigit(month);
                        String dayString = checkDigit(dayOfMonth);
                        String dateString = yearString + "-" + monthString  + "-" + dayString;
                        fromDate = dateFormat.parse(dateString);
                        fromDateButton.setText(dateString);
                        UpdateBillListView();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }, year_x, month_x, day_x);
            datePickerDialog.show();
        } else if (button.getId() ==  R.id.ToDatePicker){
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        String yearString = checkDigit(year);
                        String monthString = checkDigit(month);
                        String dayString = checkDigit(dayOfMonth);
                        String dateString = yearString + "-" + monthString  + "-" + dayString;
                        toDate = dateFormat.parse(dateString);
                        toDateButton.setText(dateString);
                        UpdateBillListView();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }, year_x, month_x, day_x);
            datePickerDialog.show();
        } else {
            return;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bills, container, false);
        listView = view.findViewById(R.id.BillsList);
        fromDateButton = view.findViewById(R.id.FromDatePicker);
        fromDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickOnClick(v);
            }
        });
        toDateButton = view.findViewById(R.id.ToDatePicker);
        toDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickOnClick(v);
            }
        });

        DatabaseHelper.getInstance().StartUpdatingPayments(new Listener() {
            @Override
            public void onResponse(Object response) {
                UpdateBillListView();
            }
        });

        return view;
    }

    void UpdateBillListView () {

        List<Payment> payments = new ArrayList<>();

        for (Payment payment : Payments.getInstance().payments) {
            if(!((toDate != null && payment.isLaterThan(toDate)) || (fromDate != null && payment.isEarlierThan(fromDate)))) {
                payments.add(payment);

            }
        }

        listView.setAdapter(new BillListAdapter(payments, Objects.requireNonNull(getContext())));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }
}
