package elro1616students.ju.lakebaikal;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Payment {
    public String road;
    public float amount;
    public Date date;
    public String paymentId;
    public String paymentMethod;
    public String paymentSuccess;

    Payment(String paymentSuccess, String paymentMethod, String road, float amount, Date date) {
        this.paymentMethod = paymentMethod;
        this.paymentSuccess = paymentSuccess;
        this.paymentId = UUID.randomUUID().toString();
        this.road = road;
        this.amount = amount;
        this.date = date;
    }

    public boolean isEarlierThan (Date date) {
        return this.date.before(date);
    }

    public boolean isLaterThan (Date date) {
        return this.date.after(date);
    }

    Payment(String paymentSuccess, String paymentMethod, String paymentId, String road, float amount, Date date) {
        this.paymentMethod = paymentMethod;
        this.paymentSuccess = paymentSuccess;
        this.paymentId = paymentId;
        this.road = road;
        this.amount = amount;
        this.date = date;
    }

    Payment (Toll toll) {
        this.amount = toll.amount;
        this.date = Calendar.getInstance().getTime();
        this.paymentSuccess = "true";
        this.road = toll.road;
        this.paymentId = UUID.randomUUID().toString();
        this.paymentMethod = "card";

    }

    Payment () {}
}
