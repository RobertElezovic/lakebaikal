package elro1616students.ju.lakebaikal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class DatabaseHelper {

    private DatabaseReference mUser;
    private DatabaseReference mPayments;
    private DatabaseReference mTolls;
    private FirebaseAuth mAuth;

    private static final DatabaseHelper ourInstance = new DatabaseHelper();

    static DatabaseHelper getInstance() {
        return ourInstance;
    }

    private DatabaseHelper() {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        mUser = mDatabase.getReference("User");
        mPayments = mDatabase.getReference("Payments");
        mAuth = FirebaseAuth.getInstance();
        mTolls = mDatabase.getReference("Tolls");
    }

    void writeUserToDb(User user) {
        HashMap<String,Boolean> params = new HashMap<>();
        params.put("HasPaymentMethod", user.HasPaymentMethod);
        mUser.child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid()).setValue(params, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    public void signOut(){
        mAuth.signOut();
    }

    public void changePassword (String newPassword, final Listener listener) {
        final FirebaseUser user = mAuth.getCurrentUser();
        user.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    listener.onResponse(null);
                }
            }
        });
    }


    public void changeEmail (String newEmail, final Listener listener) {
        final FirebaseUser user = mAuth.getCurrentUser();
        user.updateEmail(newEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    listener.onResponse(null);
                }
            }
        });
    }

    public void deleteUser (final Listener listener){
        final FirebaseUser user = mAuth.getCurrentUser();
        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mUser.child(user.getUid()).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        mPayments.child(user.getUid()).removeValue(new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                listener.onResponse(null);
                            }
                        });
                    }
                });
            }
        });
    }

    // gets user and loads into user singleton
    void startUpdatingCurrentUser(final Listener listener) {

        mUser.orderByKey().equalTo(mAuth.getCurrentUser().getUid()).limitToFirst(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User.getInstance().firebaseUser = mAuth.getCurrentUser();
                    if(dataSnapshot.hasChildren()) {
                        User.getInstance().HasPaymentMethod = (Boolean) dataSnapshot.getChildren().iterator().next().child("HasPaymentMethod").getValue();
                    } else {
                        User.getInstance().HasPaymentMethod = false;
                    }
                    listener.onResponse(null);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void StartUpdatingTolls(final Listener listener) {
        mTolls.orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getKey());
                for (DataSnapshot rawToll: dataSnapshot.getChildren()) {

                    Toll toll = rawToll.getValue(Toll.class);
                    toll.uuid = rawToll.getKey();
                    Tolls.getInstance().tolls.add(toll);
                }
                listener.onResponse(null);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void PostPayment(Payment payment, final Listener listener) {
        mPayments.child(mAuth.getCurrentUser().getUid()).child(payment.paymentId).setValue(payment, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if(databaseError == null) {
                    if(listener != null)
                        listener.onResponse(null);
                } else {
                    if(listener != null)
                        listener.onResponse(databaseError);
                }
            }
        });
    }

    public void StartUpdatingPayments (final Listener listener) {
        mPayments.child(mAuth.getCurrentUser().getUid()).orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Payments.getInstance().payments.clear();
                for (DataSnapshot paymentSnap : dataSnapshot.getChildren()) {
                    Payment payment = paymentSnap.getValue(Payment.class);
                    Payments.getInstance().payments.add(payment);
                }
                listener.onResponse(null);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
