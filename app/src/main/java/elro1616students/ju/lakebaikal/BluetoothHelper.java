package elro1616students.ju.lakebaikal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.neovisionaries.bluetooth.ble.advertising.ADPayloadParser;
import com.neovisionaries.bluetooth.ble.advertising.ADStructure;
import com.neovisionaries.bluetooth.ble.advertising.IBeacon;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

class BTAd {
    public Date timeDetected;
    public String uuid;

    public BTAd(Date timeDetected, String uuid) {
        this.timeDetected = timeDetected;
        this.uuid = uuid;
    }
}

class BluetoothHelper {

    BluetoothAdapter btAdapter;
    private Context context;
    private boolean mScanning = true;
    private BTAd lastAd;
    private Listener listener;


    private BluetoothAdapter.LeScanCallback callback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            List<ADStructure> structureList = ADPayloadParser.getInstance().parse(scanRecord);

            for (ADStructure structure : structureList) {
                if (structure instanceof IBeacon) {
                    IBeacon iBeacon = (IBeacon) structure;
                    String uuid = iBeacon.getUUID().toString();
                    int major = iBeacon.getMajor();
                    int minor = iBeacon.getMinor();
                    int power = iBeacon.getPower();

                    for (final Toll toll : Tolls.getInstance().tolls) {
                        if (uuid.equals(toll.uuid)) {
                            Date currentDate = Calendar.getInstance().getTime();
                            if(lastAd == null || !lastAd.uuid.equals(uuid) || (currentDate.getTime() - lastAd.timeDetected.getTime())/60000 > 10) {
                                DatabaseHelper.getInstance().PostPayment(new Payment(toll), new Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        Toast.makeText(context,"Payment sent", Toast.LENGTH_SHORT).show();
                                        //TODO send notification and maybe some graphical
                                        listener.onResponse(toll.amount);
                                    }
                                });
                            }
                            lastAd = new BTAd(currentDate, uuid);
                        }
                    }
                }
            }
        }
    };


    BluetoothHelper(Context context,  BluetoothAdapter btAdapter, Listener listener) {
        this.btAdapter = btAdapter;
        this.context = context;
        this.listener = listener;
    }

    void scanLeDevice(final boolean enable) {

        if (enable) {
            mScanning = true;
            System.out.println("BT START SCAN");
            btAdapter.startLeScan(callback);
        } else {
            System.out.println("BT STOP SCAN");
            mScanning = false;
            btAdapter.stopLeScan(callback);
        }
    }
}
