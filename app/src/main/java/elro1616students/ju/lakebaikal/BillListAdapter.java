package elro1616students.ju.lakebaikal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class BillListAdapter extends BaseAdapter {

    private List<Payment> payments;
    private LayoutInflater layoutInflater;

    BillListAdapter(List<Payment> payments, Context mContext) {
        this.payments = payments;
        this.layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return payments.size();
    }

    @Override
    public Object getItem(int position) {
        return payments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView =  layoutInflater.inflate(R.layout.bill_list_item, parent, false);
        }

        TextView amountText = convertView.findViewById(R.id.AmountText);
        amountText.setText(String.valueOf(payments.get(position).amount) + " kr");
        TextView roadText = convertView.findViewById(R.id.RoadText);
        roadText.setText(payments.get(position).road);
        TextView dateText = convertView.findViewById(R.id.DateText);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(payments.get(position).date);
        dateText.setText(dateString);
        return convertView;
    }
}
