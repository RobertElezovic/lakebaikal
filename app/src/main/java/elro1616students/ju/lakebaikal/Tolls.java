package elro1616students.ju.lakebaikal;

import java.util.ArrayList;
import java.util.List;

class Tolls {
    private static final Tolls ourInstance = new Tolls();

    static Tolls getInstance() {
        return ourInstance;
    }

    public List<Toll> tolls;

    private Tolls() {
        tolls = new ArrayList<>();
    }
}
